package ua.levelup.baranov;

import java.io.File;

import ua.levelup.baranov.exception.FileOrFolderException;
import ua.levelup.baranov.view.ConsoleView;
import ua.levelup.baranov.view.View;
import ua.levelup.baranov.view.ViewResolver;

public class Dispatcher {

    private String textForEach;
    private String pathForEach;
    private View console;
    private Handler handler;
    private ViewResolver viewResolver;

    Dispatcher(String textForEach, String pathForEach) {
        this.textForEach = textForEach;
        this.pathForEach = pathForEach;
        this.handler = new Handler();
        this.console = new ConsoleView();
        this.viewResolver = new ViewResolver();
    }

    public void dispatch() throws FileOrFolderException {

        File pathForEach = new File(this.pathForEach);

        if (pathForEach.isFile()) {
            console.print(viewResolver.resolve(handler.handle(pathForEach, textForEach),
                    pathForEach, textForEach));
        }

        if (pathForEach.isDirectory()) {

            File[] arrayFiles = pathForEach.listFiles();

            if (arrayFiles == null) {
                throw new FileOrFolderException("There are no files in the directory");
            } else {
                for (File direcFile : arrayFiles) {
                    if (direcFile.isFile()) {
                        console.print(viewResolver.resolve(handler.handle(direcFile, textForEach),
                                pathForEach, textForEach));
                    }
                }
            }
        }
    }
}
