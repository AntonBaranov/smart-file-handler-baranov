package ua.levelup.baranov;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import ua.levelup.baranov.parser.Parser;
import ua.levelup.baranov.parser.TxtParser;
import ua.levelup.baranov.parser.XmlParser;


class Handler {

    private List<Parser> parsers = new ArrayList<>();

    Handler() {
        parsers.add(new TxtParser());
        parsers.add(new XmlParser());
    }

    Result handle(File file, String textForEach) {

        Result result = new Result();

        for (Parser parser : parsers) {
            if (parser.isSupportFile(file.getPath())) {
                result.merge(parser.parse(file, textForEach));
            }
        }
        return result;
    }
}
