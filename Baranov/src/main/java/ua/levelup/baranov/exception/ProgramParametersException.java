package ua.levelup.baranov.exception;

public class ProgramParametersException extends Exception {

    public ProgramParametersException(String messageFromException) {
        super(messageFromException);
    }
}
