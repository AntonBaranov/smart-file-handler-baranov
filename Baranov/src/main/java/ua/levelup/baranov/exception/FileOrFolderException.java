package ua.levelup.baranov.exception;

public class FileOrFolderException extends Exception {

    public FileOrFolderException(String messageFromException) {
        super(messageFromException);
    }
}
