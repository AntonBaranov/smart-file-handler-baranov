package ua.levelup.baranov;

import java.util.ArrayList;
import java.util.List;

public class Result {

    private Integer totalCount = 0;

    private List<String> matches = new ArrayList<>();

    public Integer getTotalCount() {
        return totalCount;
    }

    public List<String> getMatches() {
        return matches;
    }

    public void addMatch(String match) {
        matches.add(match);
        totalCount++;
    }

    public void merge(Result result) {
        matches.addAll(result.getMatches());
        totalCount = matches.size();
    }

    public String toString() {

        StringBuilder resultStringBuilder = new StringBuilder();
        for (String line : matches) {
            resultStringBuilder.append("-> ").append(line + "\n");
        }
        return resultStringBuilder.toString();
    }
}