package ua.levelup.baranov.parser;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

class FileReader {

    List<String> readLines(File file) throws IOException {

        List<String> lines = new ArrayList<>();

        try (BufferedReader bufferedReader = new BufferedReader(new java.io.FileReader(file))) {

            String lineFile;

            while ((lineFile = bufferedReader.readLine()) != null) {
                lines.add(lineFile);
            }
        }
        return lines;
    }
}
