package ua.levelup.baranov.parser;

import java.io.File;

import ua.levelup.baranov.Result;


public class XmlParser implements Parser {
    @Override
    public Result parse(File file, String string) {
        return null;
    }

    @Override
    public boolean isSupportFile(String pathFile) {
        if (pathFile.endsWith(".xml")) {
            return true;
        }
        return false;
    }
}
