package ua.levelup.baranov.parser;

import java.io.File;
import java.io.IOException;
import java.util.List;

import ua.levelup.baranov.Result;


public class TxtParser implements Parser {

    private FileReader fileReader = new FileReader();

    @Override
    public Result parse(File file, String textForEach) {

        Result matches = new Result();

        try {
            List<String> fileReaded = fileReader.readLines(file);

            for (String lineFileReaded : fileReaded) {

                if (lineFileReaded.contains(textForEach)) {
                    matches.addMatch(lineFileReaded);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return matches;
    }

    @Override
    public boolean isSupportFile(String pathForEach) {
        if (pathForEach.endsWith(".txt")) {
            return true;
        }
        return false;
    }
}
