package ua.levelup.baranov.parser;

import java.io.File;

import ua.levelup.baranov.Result;

public interface Parser {

    Result parse(File file, String string);

    boolean isSupportFile(String string);
}
