package ua.levelup.baranov.view;

import java.io.File;

import ua.levelup.baranov.Result;

public class ViewResolver {

    private final String template = "Result for text " + "'%s'" + "\n"
            + "Search: %s" + "\n" + "Total matches: %s\n" + "%s";

    public String resolve(Result result, File file, String textForEach) {
        return String.format(template, textForEach, file.getPath(),
                result.getTotalCount(), result.toString());
    }
}
