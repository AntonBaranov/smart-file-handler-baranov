package ua.levelup.baranov.view;

public class ConsoleView implements View {

    @Override
    public void print(String string) {
        System.out.println(string);
    }
}
