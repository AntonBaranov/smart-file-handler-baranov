package ua.levelup.baranov.view;

public interface View {

    void print(String string);
}
