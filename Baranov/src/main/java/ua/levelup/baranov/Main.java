/*
 * Класс меин - старт программы, проверка параметров
 */

package ua.levelup.baranov;

import java.io.File;

import ua.levelup.baranov.exception.FileOrFolderException;
import ua.levelup.baranov.exception.ProgramParametersException;

public class Main {
    public static void main(String[] args) {

        String textForEach;

        String pathForEach;

        try {
            if (args.length < 2) {
                throw new ProgramParametersException("A minimum of two parameters is required.");
            } else {
                textForEach = args[args.length - 2];
                pathForEach = args[args.length - 1];
            }
            if (textForEach == null || textForEach.trim().length() == 0) {
                throw new ProgramParametersException("Search text must exist");
            }
            if (pathForEach == null || pathForEach.trim().length() == 0) {
                throw new ProgramParametersException("Path for search must exist");
            }
            if (!new File(pathForEach).exists()) {
                throw new FileOrFolderException(
                        "The file/directory does not exist at the specified path");
            } else {

                Dispatcher dispatcher = new Dispatcher(textForEach, pathForEach);

                dispatcher.dispatch();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}